/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katwekibs.kibsjavaprinter.demo;

import static com.katwekibs.kibsjavaprinter.MyComponent.BORDER_ON;
import com.katwekibs.kibsjavaprinter.Page;
import static com.katwekibs.kibsjavaprinter.Page.Page;
import com.katwekibs.kibsjavaprinter.Paragraph;
import static com.katwekibs.kibsjavaprinter.Paragraph.Paragraph;
import com.katwekibs.kibsjavaprinter.PrinterObj;
import com.katwekibs.kibsjavaprinter.ReportPrinter;
import com.katwekibs.kibsjavaprinter.Table;
import static com.katwekibs.kibsjavaprinter.Table.Table;
import com.katwekibs.kibsjavaprinter.TableColumn;
import static com.katwekibs.kibsjavaprinter.TableColumn.TableColumn;
import com.katwekibs.kibsjavaprinter.TableRow;
import static com.katwekibs.kibsjavaprinter.TableRow.TableRow;
import java.awt.Font;

/**
 *
 * @author mo
 */
public class PrinterDemo {

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PrinterDemo();
            }
        });
    }

    public PrinterDemo() {
        Page page = Page()
                .setFont(Font.MONOSPACED)
                .setMarginPercents(7);

        Paragraph header = Paragraph("This is a test print")
                .setBottomMargin(50)
                .setMarginPercents(2);
        //header.setFontSize(11);
        //header.setAlignment(Paragraph.ALIGN_CENTER);
        //header.setBottomLine(false);

        Paragraph street = Paragraph("Column 11 ");

        Paragraph suburb = Paragraph("Column 12 Last  the most last paragraho in the whole world wide in the classe of ninity");

        Paragraph town = Paragraph("Column 13");

        Paragraph kkkl = Paragraph("Column 14");

        Paragraph ppo = Paragraph("Column 15 Column 12 Last  the most last paragraho in the whole world wide in the classe of ninity");

        Paragraph header1 = Paragraph("Column 21");

        Paragraph email = new Paragraph("Column 23 Column  Last  the most last paragraho in the whole world wide in the classe of ninity");

        Paragraph dd = new Paragraph("Column 23 Column 12 Last  the most last paragraho in the whole world wide in the classe of ninity");

        TableColumn column = TableColumn()
                .setWidthPercentage(50)
                .setRightMarginPercentage(20)
                .setBorderBotom(BORDER_ON);

        TableColumn column2 = TableColumn()
                .setWidthPercentage(50);

        column.addComponent(street)
                .addComponent(suburb)
                .addComponent(town)
                .addComponent(kkkl)
                .addComponent(ppo);

        column2.addComponent(header1)
                .addComponent(email)
                .addComponent(dd);

        TableRow vatRow = TableRow()
                .addComponent(column)
                .addComponent(column2);

        Paragraph eea = Paragraph("Last  the most last paragraho in the whole world wide in the classe of ninity");

        page.addComponent(header)
                .addComponent(vatRow)
                .addComponent(eea);

        PrinterObj printer = new ReportPrinter();
        printer.performPrint(page);
    }

}
