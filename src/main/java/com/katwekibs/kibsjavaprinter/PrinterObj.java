/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katwekibs.kibsjavaprinter;

import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import javax.print.PrintService;

/**
 *
 * @author muhamedkakembo
 */
public abstract class PrinterObj {

    protected String printerName;
    protected PageFormat pageFormat;

    public abstract void performPrint(Printable doc);

    public abstract PrintService getPrintService();

    protected void setPrinterJob(PrinterJob job) {
        PrintService printer = getPrintService();
        if (printer != null) {
            try {
                job.setPrintService(printer);
            } catch (PrinterException ex) {
                System.err.println("could not connect to a printer " + printer.getName());
            }
        }
    }
    
    protected void setPageFormat(PrinterJob job){
        this.pageFormat = job.defaultPage();
    }
}
