/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katwekibs.kibsjavaprinter;

import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.Stroke;

/**
 *
 * @author muhamedkakembo
 */
public class Line extends MyComponent {

    private String lineType = Line.SOLID;
    private float lineDepth = (float) 0.5;
    private float vissibleDashWidth = 2;
    private float inVissibleDashWidth = (float) 1.5;
    private boolean drawDaubleLine = false;
    private float doubleLineStrut = lineDepth * 5;
    //
    //
    public static final String SOLID = "solid";
    public static final String DASHED = "dashed";

    public Line() {
        setMarginPercents(0);
    }

    @Override
    public void drawBorder(Graphics2D canvas, float imageableWidth, float imageableHeigt, float imageableX, float imageableY) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void drawData(Graphics2D canvas, float imageableWidth, float imageableHeight, float imageableX, float imageableY) {
        this.setWidth(this.calculatePecentageSize(this.getWidthPercentage(), imageableWidth));
        setMargins();
        
        this.setImageableX(this.getLeftMargin() + imageableX);
        this.setImageableY(this.getTopMargin() + imageableY);
        this.setImageableHeight(imageableHeight - this.getImageableY() - this.getBottomMargin());


        canvas.setStroke(this.getStroke());
        canvas.drawLine((int) this.getImageableX(), (int) this.getImageableY(), (int) this.getImageableX() + (int) this.getImageableWidth(), (int) this.getImageableY());
        if (isDrawDaubleLine()) {
            this.setImageableY(this.getImageableY() + (int) this.getDoubleLineStrut());
            canvas.drawLine((int) this.getImageableX(), (int) this.getImageableY(), (int) this.getImageableX() + (int) this.getImageableWidth(), (int) this.getImageableY());
        }
        this.setImageableY(this.getImageableY() + (this.getFontSize() + (this.getFontSize() / 2)) + this.getBottomMargin());
    }

    public String getLineType() {
        return lineType;
    }

    public void setLineType(String lineType) {
        this.lineType = lineType;
    }

    public float getLineDepth() {
        return lineDepth;
    }

    public void setLineDepth(float lineDepth) {
        this.lineDepth = lineDepth;
    }

    public float getVissibleDashWidth() {
        return vissibleDashWidth;
    }

    public void setVissibleDashWidth(float vissibleDashWidth) {
        this.vissibleDashWidth = vissibleDashWidth;
    }

    public float getInVissibleDashWidth() {
        return inVissibleDashWidth;
    }

    public void setInVissibleDashWidth(float inVissibleDashWidth) {
        this.inVissibleDashWidth = inVissibleDashWidth;
    }

    public boolean isDrawDaubleLine() {
        return drawDaubleLine;
    }

    public void setDrawDaubleLine(boolean drawDaubleLine) {
        this.drawDaubleLine = drawDaubleLine;
    }

    public float getDoubleLineStrut() {
        return doubleLineStrut;
    }

    public void setDoubleLineStrut(float doubleLineStrut) {
        this.doubleLineStrut = doubleLineStrut;
    }

    private Stroke getStroke() {
        Stroke stroke;
        if (this.getLineType().equals(Line.DASHED)) {
            stroke = new BasicStroke(this.getLineDepth(), BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]{this.vissibleDashWidth, this.inVissibleDashWidth}, 0);
        } else {
            stroke = new BasicStroke(this.getLineDepth());
        }
        return stroke;
    }
}
