/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katwekibs.kibsjavaprinter;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;

/**
 *
 * @author muhamedkakembo
 */
public class TableRow extends MyComponent<TableRow> {

    public TableRow() {
        setMarginPercents(0);
    }

     public static TableRow TableRow() {
        return new TableRow();
    }
     
    @Override
    public void drawBorder(Graphics2D canvas, float imageableWidth, float imageableHeigt, float imageableX, float imageableY) {
        canvas.setPaint(Color.GRAY);
        canvas.setStroke(new BasicStroke(1));
        canvas.drawLine((int) imageableX, (int) imageableY, (int) imageableX + (int) imageableWidth, (int) imageableY);
        canvas.setPaint(Color.BLACK);
    }

    @Override
    public void drawData(Graphics2D canvas, float imageableWidth, float imageableHeight, float imageableX, float imageableY) {

        this.setWidth(this.calculatePecentageSize(this.getWidthPercentage(), imageableWidth));
        setMargins();
        
        this.setImageableX(this.getLeftMargin() + imageableX);
        this.setImageableY(this.getTopMargin() + imageableY);
        this.setImageableHeight(imageableHeight - this.getImageableY() - this.getBottomMargin());

        float origin = this.getImageableX();
        if (this.getBorderTop() == 1) {
            int fontSize = this.getFontSize();
            this.setImageableY(this.getImageableY() - (fontSize / 2));
            this.drawBorder(canvas, this.getImageableWidth(), this.getImageableHeight(), this.getImageableX(), this.getImageableY());
            this.setImageableY(this.getImageableY() + (fontSize));
            this.setLength(this.getLength() + fontSize);
            this.setImageableHeight(this.getImageableHeight() - fontSize);
        }
        if (this.getComponents().size() > 0) {
            float[] imageableYs = new float[getComponents().size()];
            for (int i = 0; i < this.getComponents().size(); i++) {
                MyComponent cell = this.getComponents().get(i);
                cell.drawData(canvas, this.getImageableWidth(), this.getImageableHeight(), this.getImageableX(), this.getImageableY());

                this.setImageableX(this.getImageableX() + cell.getWidth());
                imageableYs[i] = cell.getImageableY();

            }
            this.setImageableY(getMax(imageableYs));
        }
        int fontSize = this.getFontSize();
        if (this.getBorderBotom() == 1) {
            this.setImageableY(this.getImageableY() - (fontSize / 2));
            this.drawBorder(canvas, this.getImageableWidth(), this.getImageableHeight(), origin, this.getImageableY());
            this.setImageableY(this.getImageableY() + (fontSize + (fontSize / 2)));
        }

        if (this.isBottomLine()) {
            this.setImageableY(this.getImageableY() + this.getFontSize());
        }
    }

    private float getMax(float[] args) {
        float max = args[0];
        for (float value : args) {
            if (value > max) {
                max = value;
            }
        }
        return max;
    }
}
