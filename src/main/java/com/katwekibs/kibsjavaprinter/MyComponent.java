/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katwekibs.kibsjavaprinter;

import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.font.TextLayout;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author muhamedkakembo
 */
public abstract class MyComponent<T extends MyComponent> {

    public static final String DEFAULT_FONT = "Serif";
    public static final int DEFAULT_FONT_SIZE = 10;
    public static final String DEFAULT_ALIGNMENT = "left";
    public static final float DEFAULT_WIDTH_PERCENTAGE = 100;
    public static final float DEFAULT_LEFT_MARGIN_PERCENTAGE = 7;
    public static final float DEFAULT_RGIHT_MARGIN_PERCENTAGE = 7;
    public static final float DEFAULT_BOTTOM_MARGIN_PERCENTAGE = 7;
    public static final float DEFAULT_TOP_MARGIN_PERCENTAGE = 7;
    //
    public static final String ALIGN_LEFT = "left";
    public static final String ALIGN_RIGHT = "right";
    public static final String ALIGN_CENTER = "center";
    public static final String ALIGN_JUSTIFIED = "justified";
    public static final int SYTLE_BOLD = 1;
    public static final int SYTLE_ITALIC = 1;
    public static final int SYTLE_UNDERLINE = 1;
    public static final int BORDER_ON = 1;
    //
    protected Object data;
    protected int fontSize = 10;
    protected Font font = new Font("Serif", Font.PLAIN, this.fontSize);
    protected int fontWeight = 0;
    protected int fontStyle = 0;
    protected int underLine = 0;
    protected int borderTop = 0;
    protected int borderBotom = 0;
    protected boolean bottomLine = false;
    protected String alignment = MyComponent.ALIGN_LEFT;
    protected float imageableHeight = 0;
    protected float length = 0;
    protected float widthPercentage = 100; // %
    protected float width;
    protected float imageableX;
    protected float imageableY;
    //
    // margins are culculated in percentage
    protected float leftMarginPercentage = 7; // %    
    protected float topMarginPercentage = 7; // %
    protected float rightMarginPercentage = 7; // %
    protected float bottomMarginPercentage = 7; // %
    protected float leftMargin;
    protected float topMargin;
    protected float rightMargin;
    protected float bottomMargin;
    protected float baseLine = 0;
    //
    protected MyComponent parent;
    protected MyComponent setting;
    protected List<MyComponent> components = new LinkedList<>();

    public MyComponent() {
    }

    protected abstract void drawData(Graphics2D canvas, float imageableWidth, float imageableHeight, float imageableX, float imageableY);

    public T setData(Object text) {
        this.data = text;
        return (T) this;
    }
  
    
    public T setBaseline() {
        this.baseLine = 0;
        return (T) this;
    }
    
    protected abstract void drawBorder(Graphics2D canvas, float imageableWidth, float imageableHeigt, float imageableX, float imageableY);

    public T addComponent(MyComponent component) {
        this.components.add(component);
        component.setParent(this);
        component.afterAddEvent();
        
        return (T) this;
    }

    public T setFont(String fontName) {
        this.font = new Font(fontName, Font.PLAIN, this.getFontSize());
        return (T) this;
    }

    public T setFontSize(int num) {
        String currentFontName = this.font.getName();
        this.font = new Font(currentFontName, Font.PLAIN, (int) num);
        this.fontSize = num;
         return (T) this;
    }

    public T setAlignment(String align) {
        this.alignment = align;
         return (T) this;
    }

    public T setBottomMargin(float bottomMargin) {
        this.bottomMargin = bottomMargin;
         return (T) this;
    }

    public T setRightMargin(float rightMargin) {
        this.rightMargin = rightMargin;
         return (T) this;
    }

    public T setTopMargin(float topMargin) {
        this.topMargin = topMargin;
         return (T) this;
    }

    public T setLeftMargin(float leftMargin) {
        this.leftMargin = leftMargin;
         return (T) this;
    }

    public T setWidthPercentage(float width) {
        this.widthPercentage = width;
         return (T) this;
    }

    public T setLeftMarginPercentage(float leftMarginPercentage) {
        this.leftMarginPercentage = leftMarginPercentage;
         return (T) this;
    }

    public T setTopMarginPercentage(float topMarginPercentage) {
        this.topMarginPercentage = topMarginPercentage;
         return (T) this;
    }

    public T setRightMarginPercentage(float rightMarginPercentage) {
        this.rightMarginPercentage = rightMarginPercentage;
         return (T) this;
    }

    public T setBottomMarginPercentage(float bottomMarginPercentage) {
        this.bottomMarginPercentage = bottomMarginPercentage;
         return (T) this;
    }

    public T setMarginPercents(float margin) {
        this.setLeftMarginPercentage(margin);
        this.setTopMarginPercentage(margin);
        this.setRightMarginPercentage(margin);
        this.setBottomMarginPercentage(margin);
         return (T) this;
    }

    protected void setMargins(float margin) {
        this.setTopMargin(margin);
        this.setRightMargin(margin);
        this.setBottomMargin(margin);
        this.setLeftMargin(margin);
    }

    protected void setWidth(float width) {
        this.width = width;
    }

    protected void setImageableHeight(float height) {
        this.imageableHeight = height;
    }

    public T setImageableX(float imageableX) {
        this.imageableX = imageableX;
         return (T) this;
    }

    public T setImageableY(float imageableY) {
        this.imageableY = imageableY;
         return (T) this;
    }

    public T setFontWeight(int fontWeight) {
        this.fontWeight = fontWeight;
         return (T) this;
    }

    public T setFontStyle(int fontStyle) {
        this.fontStyle = fontStyle;
         return (T) this;
    }

    public T setUnderLine(int underLine) {
        this.underLine = underLine;
         return (T) this;
    }

    public T setBorderBotom(int borderBotom) {
        this.borderBotom = borderBotom;
         return (T) this;
    }

    public T setBorderTop(int borderTop) {
        this.borderTop = borderTop;
         return (T) this;
    }

    public T setLength(float length) {
        this.length = length;
         return (T) this;
    }

    public MyComponent getParent() {
        return this.parent;
    }

    public List<MyComponent> getComponents() {
        return this.components;
    }

    public float getWidthPercentage() {
        return this.widthPercentage;
    }

    public String getAlignment() {
        return alignment;
    }

    public int getFontSize() {
        return fontSize;
    }

    public Font getFont() {
        return font;
    }

    public Object getData() {
        return this.data;
    }

    public float getImageableHeight() {
        return this.imageableHeight;
    }

    public float getLeftMarginPercentage() {
        return leftMarginPercentage;
    }

    public float getTopMarginPercentage() {
        return topMarginPercentage;
    }

    public float getBottomMarginPercentage() {
        return bottomMarginPercentage;
    }

    public float getRightMarginPercentage() {
        return rightMarginPercentage;
    }

    public float getBaseLine() {
        return this.baseLine;
    }

    public float getWidth() {
        return width;
    }

    public float getLength() {
        return length;
    }

    public float getLeftMargin() {
        return leftMargin;
    }

    public float getTopMargin() {
        return topMargin;
    }

    public boolean isBottomLine() {
        return bottomLine;
    }

    public float getRightMargin() {
        return rightMargin;
    }

    public float getBottomMargin() {
        return bottomMargin;
    }

    public float getImageableX() {
        return imageableX;
    }

    public float getImageableY() {
        return imageableY;
    }

    public float getImageableWidth() {
        return this.getWidth() - this.getLeftMargin() - this.getRightMargin();
    }

    public int getFontWeight() {
        return fontWeight;
    }

    public int getFontStyle() {
        return fontStyle;
    }

    public int getUnderLine() {
        return underLine;
    }

    public int getBorderTop() {
        return borderTop;
    }

    public int getBorderBotom() {
        return borderBotom;
    }

    public MyComponent getComponentAt(int index) {
        return this.getComponents().get(index);
    }

    protected void setParent(MyComponent parent) {
        this.parent = parent;
    }

    public T setBottomLine(boolean bottomLine) {
        this.bottomLine = bottomLine;
         return (T) this;
    }

    protected void afterAddEvent() {
        this.runSettables();
    }

    public String setDefaultAlignment() {
        String result = MyComponent.ALIGN_LEFT;
        if (this.getAlignment().equals(MyComponent.ALIGN_LEFT)) {
            if (this.getParent() != null) {
                result = this.getParent().setDefaultAlignment();
            }
            this.setAlignment(result);
        }
        return this.getAlignment();
    }

    public String setDefaultFont() {
        String result = MyComponent.DEFAULT_FONT;
        if (this.getFont().getName().equals(MyComponent.DEFAULT_FONT)
                && this.getFont().getSize() == MyComponent.DEFAULT_FONT_SIZE) {
            if (this.getParent() != null) {
                result = this.getParent().setDefaultFont();
            }
            this.setFont(result);
        }
        return this.getFont().getName();
    }

    public int setDefaultFontSize() {
        int result = MyComponent.DEFAULT_FONT_SIZE;
        if (this.getFontSize() == MyComponent.DEFAULT_FONT_SIZE) {
            if (this.getParent() != null) {
                result = this.getParent().setDefaultFontSize();
            }
            this.setFontSize(result);
        }
        return this.getFontSize();
    }

    public float setDefaultWidthPercent() {
        float result = MyComponent.DEFAULT_WIDTH_PERCENTAGE;
        if (this.getWidthPercentage() == MyComponent.DEFAULT_WIDTH_PERCENTAGE) {
            if (this.getParent() != null) {
                result = this.getParent().setDefaultWidthPercent();
            }
            this.setWidthPercentage(result);
        }
        return this.getWidthPercentage();
    }

    public float setDefaultMargins() {
        float result = MyComponent.DEFAULT_LEFT_MARGIN_PERCENTAGE;
        if (this.getLeftMarginPercentage() == MyComponent.DEFAULT_LEFT_MARGIN_PERCENTAGE
                && this.getTopMarginPercentage() == MyComponent.DEFAULT_TOP_MARGIN_PERCENTAGE
                && this.getRightMarginPercentage() == MyComponent.DEFAULT_RGIHT_MARGIN_PERCENTAGE
                && this.getBottomMarginPercentage() == MyComponent.DEFAULT_BOTTOM_MARGIN_PERCENTAGE) {
            if (this.getParent() != null) {
                result = this.getParent().setDefaultMargins();
            }
            this.setMarginPercents(result);
        }
        return this.getLeftMarginPercentage();
    }

    public void runSettables() {
        this.setDefaultAlignment();
        this.setDefaultFont();
        this.setDefaultFontSize();
        this.setDefaultMargins();
        //this.setDefaultWidthPercent();
    }

    protected float alignCenter(float leftMargin, TextLayout textLayout) {
        float result = 0;
        float textWidth = (float) textLayout.getVisibleAdvance();

        result = (this.getWidth() - textWidth);

        if (result > 0) {
            result = result / 2;
        } else {
            result = 0;
        }
        return result + leftMargin;
    }

    protected float alignRight(float leftMargin, TextLayout textLayout) {
        float result = 0;
        float textWidth = (float) textLayout.getVisibleAdvance();

        result = (this.getWidth() - textWidth);
        return result + leftMargin;
    }

    protected float calculatePecentageSize(float pecentage, float parentWidth) {
        float result = 0;
        result = pecentage * parentWidth / 100;
        return result;

    }
    
    protected void setMargins(){
        if (getLeftMargin() < 1) { //if user set margins stright other than marginpercentage, then dont use margin percentage.
            this.setLeftMargin(this.calculatePecentageSize(this.getLeftMarginPercentage(), this.getWidth()));
        }

        if (getTopMargin() < 1) {
            this.setTopMargin(this.calculatePecentageSize(this.getTopMarginPercentage(), this.getWidth()));
        }
        if (getRightMargin() < 1) {
            this.setRightMargin(this.calculatePecentageSize(this.getRightMarginPercentage(), this.getWidth()));
        }

        if (getBottomMargin() < 1) {
            this.setBottomMargin(this.calculatePecentageSize(this.getBottomMarginPercentage(), this.getWidth()));
        }
    }
}
