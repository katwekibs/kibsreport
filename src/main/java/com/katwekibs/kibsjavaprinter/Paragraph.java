/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katwekibs.kibsjavaprinter;

import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.font.FontRenderContext;
import java.awt.font.LineBreakMeasurer;
import java.awt.font.TextAttribute;
import java.awt.font.TextLayout;
import java.text.AttributedCharacterIterator;
import java.text.AttributedString;
import java.util.ArrayList;

/**
 *
 * @author muhamedkakembo
 */
public class Paragraph extends MyComponent<Paragraph> {

    public Paragraph() {
        setMarginPercents(0);
    }

    public Paragraph(String data) {
        setData(data);
        setMarginPercents(0);
    }
    
    public static Paragraph Paragraph(String data) {
        return new Paragraph().setData(data);
    }

    @Override
    protected void drawBorder(Graphics2D canvas, float imageableWidth, float imageableHeigt, float imageableX, float imageableY) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void drawData(Graphics2D canvas, float imageableWidth, float imageableHeight, float imageableX, float imageableY) {
        AttributedString as = new AttributedString((String) this.getData());
        as.addAttribute(TextAttribute.FONT, this.getFont());

        this.performParagraphSettings(as);

        AttributedCharacterIterator aci = as.getIterator();
        FontRenderContext frc = canvas.getFontRenderContext();
        LineBreakMeasurer lbm = new LineBreakMeasurer(aci, frc);

        this.setWidth(this.calculatePecentageSize(this.getWidthPercentage(), imageableWidth));
        setMargins();

        this.setImageableX(this.getLeftMargin() + imageableX);
        this.setImageableY(this.getTopMargin() + imageableY);

        this.setImageableHeight(imageableHeight - this.getImageableY() - this.getBottomMargin());

        float lineHeigt = 0;
        while (lbm.getPosition() < aci.getEndIndex()) {
            float leftMargin = 0;
            leftMargin = this.getImageableX();
            TextLayout textLayout = lbm.nextLayout(this.getImageableWidth());
            switch (this.getAlignment()) {
                case Paragraph.ALIGN_JUSTIFIED:
                    textLayout = textLayout.getJustifiedLayout(this.getImageableWidth());
                    break;
                case Paragraph.ALIGN_CENTER:
                    leftMargin = this.alignCenter(leftMargin, textLayout);
                    break;
                case Paragraph.ALIGN_RIGHT:
                    leftMargin = this.alignRight(leftMargin, textLayout);
                    break;
            }

            lineHeigt = textLayout.getAscent() + textLayout.getDescent() + textLayout.getLeading();

            textLayout.draw(canvas, leftMargin, this.getImageableY());

            this.setImageableY(this.getImageableY() + lineHeigt + getBottomMargin());
            this.setImageableHeight(this.getImageableHeight() - lineHeigt);
            this.setLength(this.getLength() + lineHeigt + getBottomMargin());
        }
        if (this.isBottomLine()) {
            this.setImageableY(this.getImageableY() + lineHeigt);
        }
    }

    private boolean spaceAvailable(float lineHeight) {
        return lineHeight * 2 < this.getImageableHeight();
    }

    private void performParagraphSettings(AttributedString as) {
        if (this.getFontWeight() == MyComponent.SYTLE_BOLD) {
            as.addAttribute(TextAttribute.WEIGHT, TextAttribute.WEIGHT_DEMIBOLD);
        }

        if (this.getFontStyle() == MyComponent.SYTLE_ITALIC) {
            Font font = this.getFont();
            Font newFont = new Font(font.getName(), Font.ITALIC, this.getFontSize());
            as.addAttribute(TextAttribute.FONT, newFont);
        }

        if (this.getUnderLine() == MyComponent.SYTLE_UNDERLINE) {
            as.addAttribute(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
        }
    }

}
