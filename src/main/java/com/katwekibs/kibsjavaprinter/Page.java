/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katwekibs.kibsjavaprinter;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import static java.awt.print.Printable.NO_SUCH_PAGE;
import static java.awt.print.Printable.PAGE_EXISTS;
import java.awt.print.PrinterException;

/**
 *
 * @author muhamedkakembo
 */
public class Page extends MyComponent<Page> implements Printable {

    public Page() {

    }

    @Override
    public Page setBaseline() {
        super.setBaseline();
        this.baseLine += this.getTopMargin();
        return this;//To change body of generated methods, choose Tools | Templates.
    }

    public static Page Page() {
        return new Page();
    }

    @Override
    protected void drawBorder(Graphics2D canvas, float imageableWidth, float imageableHeigt, float imageableX, float imageableY) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void drawData(Graphics2D canvas, float imageableWidth, float imageableHeight, float imageableX, float imageableY) {
        this.setWidth(this.calculatePecentageSize(this.getWidthPercentage(), imageableWidth));

        this.setMargins(this.calculatePecentageSize(this.getLeftMarginPercentage(), this.getWidth()));

        this.setImageableX(imageableX + this.getLeftMargin());
        this.setImageableY(imageableY + this.getTopMargin());

        this.setImageableHeight(imageableHeight - this.getImageableY() - this.getBottomMargin());

        if (this.getComponents().size() > 0) {
            for (int i = 0; i < this.getComponents().size(); i++) {
                MyComponent component = (MyComponent) this.getComponents().get(i);
                component.drawData(canvas, this.getImageableWidth(), this.getImageableHeight(), this.getImageableX(), this.getImageableY());
                this.setImageableY(component.getImageableY());
                this.setImageableHeight(this.getImageableHeight() - component.getLength());
            }
        }
    }

    @Override
    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
        if (pageIndex > 0) {
            return NO_SUCH_PAGE;
        }

        Graphics2D g2 = (Graphics2D) graphics;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        this.drawData(
                g2,
                (float) pageFormat.getImageableWidth(),
                (float) pageFormat.getImageableHeight(),
                (float) pageFormat.getImageableX(),
                (float) pageFormat.getImageableY());

        return PAGE_EXISTS;
    }
}
