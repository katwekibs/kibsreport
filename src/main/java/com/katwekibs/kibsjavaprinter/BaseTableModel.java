package com.katwekibs.kibsjavaprinter;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

public abstract class BaseTableModel extends AbstractTableModel {

    private String[] columnNames;
    protected ArrayList data;

    public BaseTableModel() {
    }

    public void setData(ArrayList list) {
        this.data = list;
    }

    public ArrayList getData() {
        return data;
    }

    protected void setColumnNames(String[] columns) {
        this.columnNames = columns;
    }

    public int getColumnCount() {
        return columnNames.length;
    }

    public int getRowCount() {
        return data.size();
    }

    public String getColumnName(int col) {
        return columnNames[col];
    }

    public Object getObjectAt(int row) {
        return data.get(row);
    }

    public abstract Object getValueAt(int row, int col);

    public Class getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }

    public String[] getColumns() {
        return this.columnNames;
    }

}
