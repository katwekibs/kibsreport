/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katwekibs.kibsjavaprinter;

import java.awt.print.Book;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import javax.print.PrintService;

/**
 *
 * @author muhamedkakembo
 */
public class ReportPrinter extends PrinterObj {

    public ReportPrinter() {
    }

    @Override
    public void performPrint(Printable doc) {
        PrinterJob job = PrinterJob.getPrinterJob();
        setPageFormat(job);

        Book book = new Book();

        book.append(doc, pageFormat);

        job.setPageable(book);
        if (job.printDialog()) {
            try {
                job.print();
            } catch (PrinterException e) {
                System.err.print(e.getMessage());
            }
        }
    }

    @Override
    public PrintService getPrintService() {
        return null;
    }
}
