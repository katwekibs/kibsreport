/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katwekibs.kibsjavaprinter;

import java.awt.Graphics2D;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author muhamedkakembo
 */
public class Table extends MyComponent<Table> {
    
    private List<MyComponent> columns = new LinkedList<>();
    private MyComponent columnHeaderRow;
    private List<MyComponent> bodyRaws = new LinkedList<>();
    private float columnWidthPercentage;
    private BaseTableModel model;
    
    public Table() {
        this.columnHeaderRow = new TableRow();
    }
    
     public static Table Table() {
        return new Table();
    }
     
    protected void drawData(Graphics2D canvas, float imageableWidth, float imageableHeight, float imageableX, float imageableY) {
        this.setWidth(this.calculatePecentageSize(this.getWidthPercentage(), imageableWidth));
        setMargins();
        
        this.setImageableX(this.getLeftMargin() + imageableX);
        this.setImageableY(this.getTopMargin() + imageableY);
        this.setImageableHeight(imageableHeight - this.getImageableY() - this.getBottomMargin());
        
        if (this.columnHeaderRow != null && this.columnHeaderRow.getComponents().size() > 0) {
            columnHeaderRow.drawData(canvas, this.getImageableWidth(), this.getImageableHeight(), this.getImageableX(), this.getImageableY());
            this.setImageableY(columnHeaderRow.getImageableY());
            this.setImageableHeight(this.getImageableHeight() - columnHeaderRow.getLength());
        }
        if (this.bodyRaws.size() > 0) {
            for (int i = 0; i < this.bodyRaws.size(); i++) {
                MyComponent row = this.bodyRaws.get(i);
                // make each rows alignment smilar to column headers alignment
                for (int j = 0; j < row.getComponents().size(); j++) {
                    MyComponent c = row.getComponentAt(j);
                    c.setAlignment(this.getColumnHeader().getComponentAt(j).getAlignment());
                }
                row.drawData(canvas, this.getImageableWidth(), this.getImageableHeight(), this.getImageableX(), this.getImageableY());
                this.setImageableY(row.getImageableY());
                this.setImageableHeight(this.getImageableHeight() - row.getLength());
                this.setLength(this.getLength() + row.getLength());
            }
            if (this.isBottomLine()) {
                this.setImageableY(this.getImageableY() + this.getFontSize());
            }
        }
    }
    
    protected void drawBorder(Graphics2D canvas, float imageableWidth, float imageableHeigt, float imageableX, float imageableY) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public Table setModel(BaseTableModel model) {
        this.model = model;
        this.setTableValues();
        return this;
    }
    
    public Table setDefaultColumnWidtPercentage(int length) {
        this.columnWidthPercentage = 100 / length;
        return this;
    }
    
    public MyComponent getColumn(int index) {
        return this.columns.get(index);
    }
    
    public int getColumnCount() {
        return this.columns.size();
    }
    
    public MyComponent getColumnHeader() {
        return this.columnHeaderRow;
    }
    
    public BaseTableModel getModel() {
        return model;
    }
    
    private void setColumNames() {
        if (model.getColumnCount() > 0) {
            this.setDefaultColumnWidtPercentage(model.getColumnCount());
            for (int i = 0; i < model.getColumnCount(); i++) {
                MyComponent paragraph = new Paragraph();
                paragraph.setData(model.getColumnName(i));
                paragraph.setWidthPercentage(this.columnWidthPercentage);
                this.columns.add(paragraph);
            }
        }
    }
    
    private void setColumnHeader() {
        if (this.getColumnCount() > 0) {
            for (int i = 0; i < this.getColumnCount(); i++) {
                columnHeaderRow.addComponent(this.getColumn(i));
            }
        }
    }
    
    private void setBodyRows() {
        if (this.model.getRowCount() > 0) {
            for (int i = 0; i < model.getRowCount(); i++) {
                MyComponent row = new TableRow();
                if (this.getColumnCount() > 0) {
                    for (int column = 0; column < this.getColumnCount(); column++) {
                        MyComponent paragraph = new Paragraph();
                        paragraph.setData(this.getModel().getValueAt(i, column));
                        paragraph.setWidthPercentage(getColumn(column).getWidthPercentage());
                        paragraph.setFont(this.getFont().getName());
                        paragraph.setFontSize(this.getFontSize());
                        row.addComponent(paragraph);
                    }
                }
                this.bodyRaws.add(row);
            }
        }
    }
    
    private void setTableValues() {
        this.setColumNames();
        this.setColumnHeader();
        this.setBodyRows();
    }
}
