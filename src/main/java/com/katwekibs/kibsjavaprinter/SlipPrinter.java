/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katwekibs.kibsjavaprinter;

import java.awt.print.Book;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;

/**
 *
 * @author muhamedkakembo
 */
public class SlipPrinter extends PrinterObj {

    private boolean printeOn;

    public SlipPrinter(boolean printerOn) {
        this.printerName = "LK-TE202";
        this.printeOn = printerOn;
    }

    @Override
    public void performPrint(Printable doc) {
        PrinterJob job = PrinterJob.getPrinterJob();
        setPrinterJob(job);
        setPageFormat(job);

        Book book = new Book();

        Paper paper = new Paper();
        //paper.setSize(216, 300);

        paper.setImageableArea(0, 0, 202, pageFormat.getImageableHeight());
        if (!this.printeOn) {
            paper.setSize(216,0.1 );
            paper.setImageableArea(0, 0, 0.5, 0.4);
        }
        pageFormat.setPaper(paper);
        book.append(doc, pageFormat);

        job.setPageable(book);
        try {
            job.print();
        } catch (PrinterException e) {
            System.err.print(e.getMessage());
        }
    }

    @Override
    public PrintService getPrintService() {
        PrintService printer = null;
        PrintService[] printers = PrintServiceLookup.lookupPrintServices(null, null);
        if (printers.length > 0) {
            for (PrintService p : printers) {
                if (p.getName().equals(printerName)) {
                    printer = p;
                }
            }
        }
        return printer;
    }
}
