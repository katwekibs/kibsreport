
# kibsJavaPrinter
A tool to help you simplify the creation and printing of reports from a java programme. Create the visual parts of your reports and receipts the same way you create GUIs using Swing.

At the moment this tool uses 6 basic components.
- Page (The container of all components) 
- Table (Used to display list data)
- TableRow (Used to distribute components accros the page)
- TableColumn (Used to distribute static content across the page)
- Paragraph (Actual container for Text)
- Line (Used to draw lines)
## Usage
Download the tool's KibsJavaPrinter.jar file from the project root and add it as a dependance in your programme.

#### Create a page object
  ```sh
Page page = new Page();
page.setFont(Font.MONOSPACED);
page.setMarginPercents(7); //margins are set as percentages.
```  

or like this
  ```sh
Page page = Page().setFont(Font.MONOSPACED).setMarginPercents(7); //margins are set as percentages.
```
Since version 1.01 you can set properties of an element via method chaining as you see above.
You can configure properties at either page level or at each individual component level.


#### Create a paragraph
  ```sh
Paragraph p = new Paragraph();
p.setData("Hallo World");
p.setFontSize(11);
p.setAlignment(Paragraph.ALIGN_CENTER);
  ```

Or 

  ```sh
Paragraph p = Paragraph()
                .setData("Hallo World")
                .setFontSize(11)
                .setAlignment(Paragraph.ALIGN_CENTER);
  ```

- Again you have the liberty to configure properties at either page or any component level.
- Text data on the paragraph object is set via the setData("text") method.

#### Add our paragraph object to the page object
  ```sh
  page.addComponent(p);
  ```

#### Print
  ```sh
PrinterObj printer = new ReportPrinter();
printer.performPrint(page);
```
That simple the file will be printed by your default printer.
Of course you add as many paragraph objects as you wish to the page object.
Each paragraph object spans the width of your page.

## Print List Data


Create a table model just as you would when creating table data in swing. Make you model extend BaseTableModel.

```
public class DemoTableModel extends BaseTableModel {
    private String[] columns = {"Desc","Qty", "Unit Price", "Total"};
    public OrderDisplayTableModel() {
        super();
        this.setColumnNames(columns);
    }

    @Override
    public Object getValueAt(int row, int col) {
       ....
    }
}
```

### Create a table object
```

Table table = new Table();
table.setFont(Font.MONOSPACED);
table.setMarginPercents(0);
table.getColumnHeader().setBorderTop(Table.BORDER_ON);
table.getColumnHeader().setBorderBotom(Table.BORDER_ON);
table.getColumnHeader().setFont(Font.MONOSPACED);
table.getColumnHeader().setFontWeight(Table.SYTLE_BOLD);
table.getColumnHeader().getComponentAt(1).setAlignment(Table.ALIGN_CENTER);
table.getColumnHeader().getComponentAt(2).setAlignment(Table.ALIGN_RIGHT);
table.getColumnHeader().getComponentAt(3).setAlignment(Table.ALIGN_RIGHT);

table.getColumnHeader().setBorderBotom(Table.BORDER_ON);
```

### Create a model object 
```
DemoTableModel model = new DemoTableModel();
model.setData(new ArrayList());
table.setModel(model);
```

Add the table to pages object
```
page.addComponent(table);
```

## Print Columns of Static Data
```
public class PrinterDemo {

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PrinterDemo();
            }
        });
    }

    public PrinterDemo() {
        Page page = Page()
                .setFont(Font.MONOSPACED)
                .setMarginPercents(7);

        Paragraph header = Paragraph("This is a test print")
                .setBottomMargin(50)
                .setMarginPercents(2);
        //header.setFontSize(11);
        //header.setAlignment(Paragraph.ALIGN_CENTER);
        //header.setBottomLine(false);

        Paragraph street = Paragraph("Column 11 ");

        Paragraph suburb = Paragraph("Column 12 Last  the most last paragraho in the whole world wide in the classe of ninity");

        Paragraph town = Paragraph("Column 13");

        Paragraph kkkl = Paragraph("Column 14");

        Paragraph ppo = Paragraph("Column 15 Column 12 Last  the most last paragraho in the whole world wide in the classe of ninity");

        Paragraph header1 = Paragraph("Column 21");

        Paragraph email = new Paragraph("Column 23 Column  Last  the most last paragraho in the whole world wide in the classe of ninity");

        Paragraph dd = new Paragraph("Column 23 Column 12 Last  the most last paragraho in the whole world wide in the classe of ninity");

        TableColumn column = TableColumn()
                .setWidthPercentage(50)
                .setRightMarginPercentage(20)
                .setBorderBotom(BORDER_ON);

        TableColumn column2 = TableColumn()
                .setWidthPercentage(50);

        column.addComponent(street)
                .addComponent(suburb)
                .addComponent(town)
                .addComponent(kkkl)
                .addComponent(ppo);

        column2.addComponent(header1)
                .addComponent(email)
                .addComponent(dd);

        TableRow vatRow = TableRow()
                .addComponent(column)
                .addComponent(column2);

        Paragraph eea = Paragraph("Last  the most last paragraho in the whole world wide in the classe of ninity");

        page.addComponent(header)
                .addComponent(vatRow)
                .addComponent(eea);

        PrinterObj printer = new ReportPrinter();
        printer.performPrint(page);
    }

}
```
